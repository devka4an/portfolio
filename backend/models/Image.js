const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const imageSchema = new mongoose.Schema({
    name: {
      type: String,
      trim: true,
      required: 'Please enter a image name!'
    },
    url: {
        type: String,
        trim: true,
        required: 'Please enter a image url!'
    }
    // slug: String,
    // description: {
    //   type: String,
    //   trim: true
    // },
    // tags: [String],
    // created: {
    //   type: Date,
    //   default: Date.now
    // },
    // location: {
    //   type: {
    //     type: String,
    //     default: 'Point'
    //   },
    //   coordinates: [{
    //     type: Number,
    //     required: 'You must supply coordinates!'
    //   }],
    //   address: {
    //     type: String,
    //     required: 'You must supply an address!'
    //   }
    // },
    // photo: String,
    // author: {
    //   type: mongoose.Schema.ObjectId,
    //   ref: 'User',
    //   required: 'You must supply an author'
    // }
});

module.exports = mongoose.model('Image', imageSchema);