const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');


const postSchema = new mongoose.Schema({
    postSlug: {
        type: String,
        default: `${Date.now()}`
    },
    content: {
        type: String,
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    }
}, {
  toJSON: { virtuals: true },
  toObject: { virtuals: true },
});

module.exports = mongoose.model('Post', postSchema);