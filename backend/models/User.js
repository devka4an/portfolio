const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: 'Type your name',
        trim: true
    },
    password: {
        type: String,
        required: 'Type your password',
    },
})


module.exports = mongoose.model('User', userSchema);