const express = require('express');
const router = express.Router();

const mailer = require('./controllers/mailer')
const mainController = require('./controllers/mainController')
const telegrambot = require('./controllers/telegrambot')
const imageController = require('./controllers/imageController')
const userController = require('./controllers/userController')

router.get('/', (req, res) => {
    res.json({server: 'works'})
})

router.get('/imagelist', imageController.getImages)

router.post('/post', mainController.savePost)
router.get('/post/:id', mainController.singlePost)
router.get('/postList', mainController.postList)

router.post('/sendmemail', mailer)
router.post('/telegram', telegrambot)
router.post('/upload', 
    imageController.upload,
    imageController.imageUpload
)

router.post('/register', userController.register)
router.post('/login', userController.login)
router.post('/checkToken', userController.checkToken)

module.exports = router;