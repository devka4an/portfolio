require('dotenv').config({ path: './../.env' });

const mongoose = require('mongoose');
const path = require("path");
const express = require("express")
const bodyParser = require("body-parser")
const cors = require("cors")
const app = express()
const routes = require("./routes");
const PORT = process.env.SERVER_PORT || 9090;

mongoose.connect(process.env.DATABASE);
mongoose.Promise = global.Promise;
mongoose.connection.on('error', (err) => {
  console.error(`CONECTION ERROR TO DB ${err.message}`);
});
mongoose.connection.on('connected', () => {
  console.log(`CONNECTED TO DB`);
});


require('./models/Image');
require('./models/Post');
require('./models/User');

app.use(cors());

app.use(express.static(path.join(__dirname, "public")));
// app.use(express.static(__dirname + '/public'));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/", routes);

app.listen(PORT, () => {
    console.log(`Server started on port: ${PORT}`);
})
