const multer = require('multer')
const Image = require('./../models/Image')


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public/images/')
    },
    filename: function (req, file, cb) {
        let date = new Date()
        let ext = file.mimetype.split('/')[1]
        cb(null, `${date.getSeconds()}-${date.getMinutes()}-${date.getHours()}-${date.getDate()}-${date.getMonth()}-${date.getFullYear()}.${ext}`)
    }
})

exports.upload = multer({ 
    dest: 'public/images/',
    storage: storage
}).single('file');

exports.imageUpload = async (req, res) => {
    console.log('FILE:', req.file);
    await (new Image({
        name: req.file.filename,
        url: `/images/${req.file.filename}`
    })).save();
    res.status(200).json({
        name: req.file.filename,
        url: req.file.path
    })
}


exports.getImages = async (req, res) => {
    const images = await Image.find()
    console.log('images', images);
    res.status(200).json(images)
}