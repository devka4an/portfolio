const Post = require('./../models/Post')

exports.postList = async (req, res) => {
    const posts = await Post.find()
    res.json(posts)
}

exports.savePost = async (req, res) => {
    const post = await (new Post(req.body)).save();
    res.json(post)
}

exports.singlePost = async (req, res) => {
    const post = await Post.findOne({ postSlug: req.params.id });
    res.json(post)
}