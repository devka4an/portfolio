const User = require('./../models/User')
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


const verifyUserLogin = async (username, password)=>{
    try {
        const user = await User.findOne({username}).lean()
        if(!user){
            return {status:'error',error:'user not found'}
        }
        if(await bcrypt.compare( password, user.password)){
            // creating a JWT token
            token = jwt.sign({ 
                id:user._id, 
                username:user.username,
                type:'user'
            }, 'JWT_SECRET', {
                expiresIn: 60*10 //30
            })
            return { 
                status:'ok',
                token,
                username: user.username
            }
        }
        return {status:'error',error:'invalid password'}
    } catch (error) {
        console.log(error);
        return {status:'error',error:'timed out'}
    }
}

const verifyToken = (token)=>{
    try {
        const verify = jwt.verify(token, 'JWT_SECRET');
        console.log('verify', verify);
        if(verify.type == 'user'){
            return true;
        } else{
            return false
        };
    } catch (error) {
        console.log(JSON.stringify(error),"error");
        return false;
    }
}

exports.register = async (req, res) => {
    console.log('authenticate', req.body);

    const { username, password:textPassword } = req.body;
    const password = await bcrypt.hash(textPassword, 10);

    try {
        const response = await User.create({
            username,
            password
        })
        res.json(response)
    } catch (error) {
        console.log(JSON.stringify(error));
    }
}

exports.checkToken = async (req, res) => {
    console.log('checkToken', req.body.token);
    res.json({
        expired: !verifyToken(req.body.token)
    })
}

exports.login = async (req,res)=>{
	const { username, password } = req.body;
	// we made a function to verify our user login
    const response = await verifyUserLogin(username, password);
    if( response.status === 'ok'){
        res
            .status(200)
            .json(response)
    }else{
        res
            .status(401)
            .json(response)
    }
}
