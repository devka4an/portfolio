export { default as Home } from './Home';
export { default as About } from './About';
export { default as Contactme } from './Contactme';
export { default as Media } from './Media';
export { default as Login } from './Login';
export { default as Diary } from './Diary';