import React, { useEffect, useState } from 'react'
import { fetchPosts } from './../services'

export default function Diary() {
    const [ mode, setMode ] = useState('form')// list
    const [ list, setList ] = useState([])// list
    const formHandler = (e) => {
        e.preventDefault()
        console.log('formHandler', e);
    }

    useEffect(() => {
        fetchPosts()
            .then(res => {
                console.log('setList', res?.data);
                setList(res?.data)
            })
    }, [])
    return (
        <>
            <div className="heading">
                <h1>DIARY</h1>
                <button className="add-btn" onClick={() => setMode(mode == 'list' ? 'form' : 'list')}>{mode == 'list' ? 'ADD POST':'SHOW LIST'}</button>
            </div>
            <div className="wrapper">
                <div className="container">
                    {mode == 'form' && <form className="form full" onSubmit={formHandler} >
                        <div className="field">
                            {/* <label htmlFor="image"></label> */}
                            <textarea name="content" id="" cols="30" rows="10"></textarea>
                        </div>
                        <button className="btn" type="submit">SAVE</button>
                    </form>}
                    {mode == 'list' && <h3>list</h3>}
                </div>
            </div>
        </>
    )
}
