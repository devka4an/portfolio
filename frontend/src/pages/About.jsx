import React from 'react'
import pic1 from './../static/pic1.JPG'

export default function About() {
    return (
        <>
            <div className="heading">
                <h1>ABOUT ME</h1>
            </div>
            <div className="wrapper">
                {/* <Gallery /> */}
                {/* <div className="poster">
                    <img src={pic1} alt="" />
                </div> */}
                <div className="content">
                    <h3 className="title">Hi, my name is <span className="blue">Alisher</span>. Nice to meet you</h3>
                    <p className="aticle">
                        I'm <span className="red">frontend developer</span> from <a href="https://www.google.com/maps/place/Almaty">Kazakhstan, Almaty</a>
                    </p>
                    <p className="aticle"></p>
                </div>
            </div>
        </>
    )
}
