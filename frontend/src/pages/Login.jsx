import React, { useState } from 'react'
import { authenticate } from './../services'

export default function Login({handleNotif}) {
    const [user, setUser] = useState({
        username: '',
        password: ''
    })

    const handleUser = (e) => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        })
    }

    const login = async (e) => {
        e.preventDefault()
        try {
            const res = await authenticate(user)
            localStorage.setItem('auth', JSON.stringify({
                token: res.token
            }));
            handleNotif({
                id: new Date().getTime(),
                type: 'success',
                value: `Logged in!`
            })
        } catch (error) {
            handleNotif({
                id: new Date().getTime(),
                type: 'error',
                value: `Error on login`
            })
        }
        setUser({
            username: '',
            password: ''
        })
    }

    return (
        <>
            <h1 className="heading">LOGIN</h1>
            <div className="wrapper">
                <div className="container">
                    <form onSubmit={login} className="form login-form">
                        <div className="field">
                            <label htmlFor="username">username</label>
                            <input type="text" id="username" name="username" value={user.username} onChange={handleUser} />
                        </div>
                        <div className="field">
                            <label htmlFor="password">password</label>
                            <input type="password" id="password" name="password" value={user.password} onChange={handleUser} />
                        </div>
                        <button className="btn">LOGIN</button>
                    </form>
                </div>
            </div>
        </>
    )
}
