import React from 'react'
import { sendMail, sendTelegram } from '../services'
import icon from './../static/send-icon.svg'

export default function Contactme({handleNotif}) {
    const [loading, setLoading] = React.useState(false)
    const [form, setForm] = React.useState({
        message: '',
        sender: ''
    })

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }



    const sendme = async (e) => {
        setLoading(true)
        e.preventDefault()
        const mailP = sendMail(form)
        const mailT = sendTelegram(form)
        Promise.all([mailP, mailT])
            .then(res => {
                setLoading(false)
                setForm({
                    message: '',
                    sender: ''
                })
                handleNotif({
                    id: new Date().getTime(),
                    type: 'success',
                    value: `message sent`
                })
                console.log('Promise.all', res);
            })
            .catch(e => {
                setLoading(false)
                setForm({
                    message: '',
                    sender: ''
                })
                handleNotif({
                    id: new Date().getTime(),
                    type: 'error',
                    value: `Error on sending mail`
                })
            })
    }
    return (
        <>
            <div className="heading">
                <h1>CONTACT ME</h1>
            </div>
            <div className="wrapper">
                <div className="container">
                    {loading && <div className="loader"><div className="lds-dual-ring"></div></div>}
                    <form className="form half" onSubmit={sendme}>
                        <div className="field">
                            <label htmlFor="sender">WHO ARE U?</label>
                            <textarea name="sender" id="sender" cols="30" rows="5" onChange={handleChange} value={form.sender}></textarea>
                        </div>
                        <div className="field">
                            <label htmlFor="message">your message</label>
                            <textarea name="message" id="message" cols="30" rows="10" onChange={handleChange} value={form.message}></textarea>
                        </div>
                        <button className="btn">SEND <img src={icon} alt="" /></button>
                    </form>
                    <div className="social half">
                        <p>MY SOCIALS:</p>
                        <ul className="social_list">
                            <li className="social_item">
                                <a href="https://t.me/ka4anbek" target="_blank" rel="noreferrer">
                                    <img src="https://web.telegram.org/k/assets/img/safari-pinned-tab.svg?v=jw3mK7G9Ry" alt="telegram" />
                                </a>
                            </li>
                            <li className="social_item">
                                <a href="https://github.com/alishergani" target="_blank" rel="noreferrer">
                                    <img src="https://cdn-icons-png.flaticon.com/512/25/25657.png" alt="github" />
                                </a>
                            </li>
                            <li className="social_item">
                                <a href="https://www.instagram.com/ka4anbek/" target="_blank" rel="noreferrer">
                                    <img src="https://cdn-icons-png.flaticon.com/512/1419/1419647.png" alt="instagram" />
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </>
    )
}
