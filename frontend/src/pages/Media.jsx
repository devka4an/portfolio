import React from 'react'
import Gallery from './../comps/Gallery'
import icon from './../static/send-icon.svg'
import { checkAuth, fetchImages, uploadImages } from './../services'

export default function Media({handleNotif}) {
    const [images, setImages] = React.useState([])
    const [openForm, setOpenForm] = React.useState(false)
    const [isAuth, setAuth] = React.useState(false)
    const [state, setState] = React.useState({
        selectedFile: null,
        loaded:0
    })
    const onChangeHandler = event => {
        var files = event.target.files
        if (
            maxSelectFile(event) 
            && checkMimeType(event)
        ) {
            setState({
                selectedFile: files,
                loaded: 0
            })
        }
    }

    const checkMimeType = (event) => {
        let files = event.target.files
        let err = []
        const types = ['image/png', 'image/jpeg', 'image/gif']
        for (let x = 0; x < files.length; x++) {
            if (types.every(type => files[x].type !== type)) {
                err[x] = files[x].type + ' is not a supported format\n';
            }
        };
        for (var z = 0; z < err.length; z++) {
            console.error(err[z])
            event.target.value = null
        }
        return true;
    }
    const maxSelectFile = (event) => {
        let files = event.target.files
        if (files.length > 3) {
            const msg = 'Only 3 images can be uploaded at a time'
            event.target.value = null
            console.warn(msg)
            return false;
        }
        return true;
    }
    const onClickHandler = (e) => {
        e.preventDefault()
        if(!localStorage.getItem('auth')) {
            setAuth(false)
            handleNotif({
                id: new Date().getTime(),
                type: 'error',
                value: `Not authorized`
            })
            return
        } 
        const {expired} = checkAuth(JSON.parse(localStorage.getItem('auth')))

        if(expired) {
            setAuth(!expired)
            handleNotif({
                id: new Date().getTime(),
                type: 'error',
                value: `Not authorized`
            })
        }

        const data = new FormData()
        for (var x = 0; x < state.selectedFile.length; x++) {
            data.append('file', state.selectedFile[x])
        }
        uploadImages(data)
        .then(res => {
            // console.log('upload success');
            fetchImages().then(res => {
                setImages(res)
            })
        })
        .catch(err => {
            // console.log('upload fail');
        })
    }

    React.useEffect(() => {
        fetchImages().then(res => {
            setImages(res)
        })
        // setImages(fetchImages())
        if(!localStorage.getItem('auth')) {
            return
        } 
        const {expired} = checkAuth(JSON.parse(localStorage.getItem('auth')))
        setAuth(!expired)
    }, [])


    // const getImages = async () => {
    //     const images = await fetchImages()
    //     console.log('images', images);
    //     setImages(images)
    // }

    // React.useEffect(() => {
    // }, [])
    return (
        <>
            <div className="heading">
                <h1>MEDIA </h1>
                {isAuth && <button className="add-btn" onClick={() => setOpenForm(!openForm)}>{!openForm ? 'ADD IMAGE':'CLOSE FORM'}</button>}
            </div>
            <div className="wrapper">
                <div className="container">
                    {openForm && 
                    <>
                        <form className="form center" onSubmit={onClickHandler} >
                            <h3>ADD NEW IMAGE</h3>
                            <div className="field">
                                <label htmlFor="imgtitle">image title</label>
                                <input type="text" id="imgtitle" />
                            </div>
                            <div className="field">
                                <label htmlFor="image">choose image</label>
                                <input type="file" name="photo" onChange={onChangeHandler} accept="image/gif, image/png, image/jpeg"/>
                            </div>
                            <button className="btn" type="submit">SAVE <img src={icon} alt="" /></button>
                        </form>
                    </>
                    }
                    <Gallery images={images} />
                </div>
            </div>
        </>
    )
}
