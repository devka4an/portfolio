import axios from 'axios'
import { API_HOST, API_PORT } from './../env'

let url = `http://${API_HOST}:${API_PORT}`


export const fetchImages = () => {
    return axios
        .get(`${url}/imagelist`)
        .then(res => {
            return res.data
        })
}

export const uploadImages = (data) => {
    console.log('uploadImages', data);
    return axios
        .post(`${url}/upload`, data,{
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(res => {
            return res.data
        })
}

export const checkAuth = ({token}) => {
    // console.log('checkAuth', token);
    return axios.post(`${url}/checkToken`, {token})
}

export const authenticate = (payload) => {
    // console.log('authenticate', payload);
    return axios
        .post(`${url}/login`, payload)
        .then(res => {
            return res.data
        })
}

export const sendMail = (data) => {
    return axios.post(`${url}/sendmemail`, data,{
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        }
    })
}
export const sendTelegram = (data) => {
    return axios.post(`${url}/telegram`, data,{
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        }
    })
}

export const fetchPosts = () => axios.get(`${url}/postList`)