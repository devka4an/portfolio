let API_PORT = '7777'
let API_HOST = 'localhost'
let ENV = 'production'

export {
    API_PORT,
    API_HOST,
    ENV
}