import React from 'react'
import "./Ka4an.scss"

export default function Ka4an() {
    return (
        <div className="ka4an-wrapper">
            <div className="svg-wrapper">
                <svg className="svg-img king" id="esIgTOD9Q1V1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 200" shape-rendering="geometricPrecision" text-rendering="geometricPrecision">
                    <path
                        fill="none"
                        // stroke="green" M600 200 L0 200 L0 100 L400 100 L150 20 L600 20
                        stroke-width="40" 
                        d='
                            M600 200 L0 200 L0 100 L400 100 L150 20 L600 20
                            M600 200 L1200 200 L1200 100 L800 100 L1080 20 L600 20
                        ' 
                    />
                </svg>
                <svg className="svg-img svg-img-2" id="esIgTOD9Q1V1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 400" shape-rendering="geometricPrecision" text-rendering="geometricPrecision">
                    <path
                        fill="none"
                        // stroke="red" 
                        stroke-width="20" 
                        d='
                            M210 400 
                            L370 0 
                            L420 0 
                            L550 400 
                            L480 400 
                            L440 280 
                            L330 280 
                            L280 400 
                            L210 400 
                            L323 120 
                            L390 120 
                            L420 220 
                            L350 220 
                            L390 120' 
                    />
                </svg>
                <svg className="svg-img svg-img-4" id="esIgTOD9Q1V1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 400" shape-rendering="geometricPrecision" text-rendering="geometricPrecision">
                    <path
                        fill="none"
                        // stroke="yellow" 
                        stroke-width="20" 
                        d='
                            M210 400 
                            L370 0 
                            L420 0 
                            L550 400 
                            L480 400 
                            L440 280 
                            L330 280 
                            L280 400 
                            L210 400 
                            L323 120 
                            L390 120 
                            L420 220 
                            L350 220 
                            L390 120' 
                    />
                </svg>
                <svg className="svg-img svg-img-1" id="esIgTOD9Q1V1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 400" shape-rendering="geometricPrecision" text-rendering="geometricPrecision">
                    <path
                        fill="none"
                        // stroke="blue" 
                        stroke-width="5" 
                        // d='M0 0 L0 400 L70 400 L70 0 L0 0 L0 300 L320 0 L230 0 L70 150 L300 400 L230 400 L0 150' 
                        d='
                            M0 0 L0 400 L10 400 L10 0 L20 0 L20 400 L30 400 L30 300 L300 0 L290 0 L30 290 L30 280 L280 0 L270 0 L30 270 L30 260 
                            L260 0 L250 0 L30 250 L240 400 L250 400 L30 240 L30 230 L260 400 L270 400 L30 220 L30 210 L280 400 L290 400 L30 200 L30 0 L40 0 L40 400 L50 400 L50 0 L60 0 L60 400' 
                    />
                </svg>
                <svg className="svg-img big svg-img-3" viewBox="0 0 1500 700" shape-rendering="geometricPrecision" text-rendering="geometricPrecision">
                    <path
                        fill="none"
                        // stroke="green" 
                        stroke-width="5" 
                        d='M800 700 L800 0 L820 0 L820 690 L780 690 L780 20 L810 20 L810 500 L510 500 L510 490 L770 490 L770 480 L510 480 L770 50 L770 500 L950 500 L950 490 L830 490 L830 480 
                        L950 480 L950 460 L750 460 L750 130 L570 440 L730 440 L730 240' 
                    />
                </svg>
                <svg className="svg-img svg-img-5" id="esIgTOD9Q1V1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 400" shape-rendering="geometricPrecision" text-rendering="geometricPrecision">
                    <path
                        fill="none"
                        // stroke="gray" 
                        stroke-width="5" 
                        d='M700 0 L700 400 L710 400 L710 0 L720 0 L720 400 L730 400 L730 0 L740 0 L740 400 L750 400 L750 0 L950 400 
                        L930 400 L760 50 L790 50 L970 400 L970 0 L980 0 L980 400 L990 400 L990 0 L1000 0 L1000 400 L1010 400 L1010 0 L1020 0 L1020 400' 
                    />
                </svg>
            </div>
            {/* 
                        M210 400 L340 0
                        M470 400 L340 0
                        M260 250 L420 250
            */}
            {/* <h1 className="ka4an">
                KA4AN
                <span className="dev"></span>
            </h1>
            <h1 className="ka4an-mobile">
                KA4AN
                <span className="dev-mobile"></span>
            </h1> */}
        </div>
    )
}
