import React from 'react'
import './Gallery.scss'
import { API_HOST, API_PORT } from './../../env'
let url = `http://${API_HOST}:${API_PORT}`

export default function Gallery({images}) {
    return (
        <div className="images">
            {images.map((item, idx) => (
                <div key={idx} className="pic">
                    <img src={`${url}/${item.url}`} alt="" />
                </div>
            ))}
        </div>
    )
}
